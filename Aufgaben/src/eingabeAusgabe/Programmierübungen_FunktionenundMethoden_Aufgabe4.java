package eingabeAusgabe;

public class Programmierübungen_FunktionenundMethoden_Aufgabe4 {
public static void main(String[] args) {
		
		double r1 = 10.0;
		double r2 = 20.0;
		
		double gesamtwiderstand= parallelschaltung(r1, r2);
		System.out.println(gesamtwiderstand); 
	}
	public static double parallelschaltung(double r1, double r2) {
		return (r1 * r2) / (r1 + r2);
	}

}