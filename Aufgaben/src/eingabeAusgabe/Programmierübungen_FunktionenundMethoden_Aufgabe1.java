package eingabeAusgabe;

public class Programmierübungen_FunktionenundMethoden_Aufgabe1 {
		   public static void main(String[] args) {
		      // (E) "Eingabe"
		      // Werte für x und y festlegen:
		      // ===========================
		      double x = 2.0;
		      double y = 4.0;
		      
		      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, (berechnung(x,y)));
		   }
	public static double berechnung(double x,double y) {
		return (x + y) / 2.0;
	}
}
