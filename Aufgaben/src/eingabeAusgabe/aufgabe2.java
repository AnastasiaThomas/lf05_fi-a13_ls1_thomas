package eingabeAusgabe;
public class aufgabe2 {

	public static void main(String[] args) {
  /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
        int counter ;
  /* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/
        counter = 25 ;
        System.out.println (counter) ;
  /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */
        String menuepunkt ;
  /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/
        menuepunkt = "C" ;
        System.out.println(menuepunkt);
  /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
        double berechnung ;
  /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/
        berechnung = 299792458 ;
        System.out.printf ("%f", berechnung) ;
  /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/
        byte mitglieder_verein ;
        mitglieder_verein = 7 ;
  /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
        System.out.printf("%b", mitglieder_verein) ;
  /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
        float elementarladung ;
        elementarladung = 1.602176634E-19f;
        System.out.println(elementarladung) ;
  /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
        boolean erfolgt ;
  /*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/
        erfolgt = true ;
        System.out.print(erfolgt);
        
		
	}

}
