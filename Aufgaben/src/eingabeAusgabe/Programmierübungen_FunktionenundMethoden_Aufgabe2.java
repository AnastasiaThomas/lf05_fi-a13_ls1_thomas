package eingabeAusgabe;



import java.util.Scanner;



public class Programmierübungen_FunktionenundMethoden_Aufgabe2 {

public static void main(String[] args) {

//Eingeben
String artikel = liesString("was möchten Sie bestellen");
int anzahl = liesInt("Geben Sie die Anzahl ein");
double preis = liesDouble("Geben Sie den Nettopreis ein: ");
double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");



// Verarbeiten
double nettoGesamtPreis = berechneGesamtNettoPreis(anzahl, preis);
double bruttoGesamtPreis = berechneGesamtBruttoPreis(nettoGesamtPreis, mwst);



// Ausgeben
rechnungAusgeben(artikel, anzahl, nettoGesamtPreis, bruttoGesamtPreis, mwst);



}

public static String liesString(String text) {
Scanner scannerString = new Scanner(System.in);
System.out.println(text);
String ausgabe = scannerString.next();
return ausgabe;
}

public static int liesInt(String text) {
Scanner scannerInt = new Scanner(System.in);
System.out.println(text);
int ausgabe = scannerInt.nextInt();
return ausgabe;
}

public static double liesDouble(String text) {
Scanner scannerDouble = new Scanner(System.in);
System.out.println(text);
double ausgabe = scannerDouble.nextDouble();
return ausgabe;

}

public static double berechneGesamtNettoPreis(int anzahl, double nettopreis) {
return anzahl*nettopreis;
}

public static double berechneGesamtBruttoPreis(double nettoGesamtPreis, double mwst) {
return nettoGesamtPreis+(nettoGesamtPreis*(mwst/100.0));
}

public static void rechnungAusgeben(String artikel, int anzahl, double nettoGesamtPreis, double bruttoGesamtPreis, double mwst) {
System.out.println("\tRechnung");
System.out.printf("\t\t Netto: %-20s %6d %10.2f %n", artikel, anzahl, nettoGesamtPreis);
System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttoGesamtPreis, mwst, "%");
}

}

