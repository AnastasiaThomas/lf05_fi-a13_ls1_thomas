import java.util.Scanner;

class FahrkartenautomatTicket
{
    public static void main(String[] args)
    {

       double zuZahlenderBetrag; 
       double r�ckgabebetrag;

       zuZahlenderBetrag=fahrkartenbestellungErfassen(); 
       
       

       // Geldeinwurf
       // ----------- 
       r�ckgabebetrag=fahrkartenBezahlen(zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben(); 

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgeldAusgeben(r�ckgabebetrag); 

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur= new Scanner(System.in);
    	
    	System.out.println("Wie viel kostet ein Ticket?");
         double einzelpreis = tastatur.nextDouble();
        
        System.out.println("Wie viele Tickets m�chten Sie kaufen?");
        short anzahltickets = tastatur.nextShort();
        
        double zuZahlenderBetrag = einzelpreis * anzahltickets;
        return zuZahlenderBetrag; 
        
        tastatur.close(); 
    }
    public static double fahrkartenBezahlen (double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.00;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f", + (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print(" Euro \nEingabe (mind. 5Ct, h�chstens 2.00 Euro): ");
     	   double eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
            
            tastatur.close(); 
            
        }
        double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
    	return r�ckgabebetrag;
    }
    public static void fahrkartenAusgeben () {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    public static void r�ckgeldAusgeben (double r�ckgabebetrag) {
    	if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f",r�ckgabebetrag,"EURO");
     	   System.out.println("� wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
            {
         	  System.out.println("2,00 EURO");
 	          r�ckgabebetrag -= 2.00;
            }
            while(r�ckgabebetrag >= 1.00) // 1 EURO-M�nzen
            {
         	  System.out.println("1,00 EURO");
 	          r�ckgabebetrag -= 1.00;
            }
            while(r�ckgabebetrag >= 0.50) // 50 CENT-M�nzen
            {
         	  System.out.println("0,50 CENT");
 	          r�ckgabebetrag -= 0.50;
            }
            while(r�ckgabebetrag >= 0.20) // 20 CENT-M�nzen
            {
         	  System.out.println("0,20 CENT");
  	          r�ckgabebetrag -= 0.20;
            }
            while(r�ckgabebetrag >= 0.10) // 10 CENT-M�nzen
            {
         	  System.out.println("0,10 CENT");
 	          r�ckgabebetrag -= 0.10;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("0,05 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }
    }
}